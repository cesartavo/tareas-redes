---
title: Documentación-Servicio de Monitoreo
author: Unammada
date: July2020
---

# Integrantes:

- 312235811	Alcantar Arenas Rogelio			rogelio-aa@ciencias.unam.mx

- 314057442	Desales Santos José Ricardo		desales@ciencias.unam.mx

- 310316521	Sánchez de la Rosa César Gustavo	cesartavo@ciencias.unam.mx

# Vídeo

[Link del vídeo del proyecto](https://youtu.be/DyEPwRoN-ck)

# Índice

- [Índice](#índice)
- [Introducción](#introducción)
- [Primeros Pasos](#primeros-pasos)
  - [Requisitos de Hardware](#requisitos-de-hardware)
  - [Modificar la conexión ssh (si se conecta a un servidor remoto)](#modificar-la-conexión-ssh-si-se-conecta-a-un-servidor-remoto)
  - [Modificar la lista de descarga de paquetes](#modificar-la-lista-de-descarga-de-paquetes)
  - [Modificar hostname del equipo](#modificar-hostname-del-equipo)
- [Requisitos para Pandora FMS](#requisitos-para-pandora-fms)
  - [Instalación de MySQL](#instalación-de-mysql)
  - [Requisitos de pandora](#requisitos-de-pandora)
  - [Configuración del archivo de la base de datos](#configuración-del-archivo-de-la-base-de-datos)
- [Instalación de Pandora FMS e inicialización de Pandora Server](#instalación-de-pandora-fms-e-inicialización-de-pandora-server)
  - [Instalación del Agente Software](#instalación-del-agente-software)
- [Instalacion de Cerbot](#instalacion-de-cerbot)
- [Instalación de alertas por Correo](#instalación-de-alertas-por-correo)
  - [Prerequisitos](#prerequisitos)
  - [Configurar postfix](#configurar-postfix)
    - [Postfix main.cf](#postfix-maincf)
  - [Configuración de una alerta por Correo en Pandora FMS](#configuración-de-una-alerta-por-correo-en-pandora-fms)
- [Alertas Telegram](#alertas-telegram)
  - [Configuración de una alerta con Telegram en Pandora FMS](#configuración-de-una-alerta-con-telegram-en-pandora-fms)
- [Alertas Twitter](#alertas-twitter)
  - [Configuración de una alerta con Twitter en Pandora FMS](#configuración-de-una-alerta-con-twitter-en-pandora-fms)
- [Pasos para monitorear un servidor](#pasos-para-monitorear-un-servidor)

# Introducción

En este proyecto vamos a levantar un servicio de monitoreo con alertas por
correo, twitter, telegram y todo lo que implica hacer estas acciones, como son:
levantar el certificado para brindar un servicio seguro y consultas por *https*
alertas por correo de manera segura sin que se manden a la bandeja de spam. 

La plataforma que vamos a usar para el monitoreo se llama **Pandora FMS** que
FMS es un acrónimo de **F**lexible **M**onitoring **S**ystem. Su propósito es poder
monitorear herramientas y sistemas complejos de nueva generación con elementos
obsoletos que tienen un acceso difícil y escasa compatibilidad, todo en una sola
plataforma.

Tiene su versión Empresarial y su versión abierta, para fines de este proyecto
se optó por la opensource; abierta al público y gratis (hasta cierto punto).

# Primeros Pasos

## [Requisitos de Hardware](https://pandorafms.com/docs/index.php?title=Pandora:Documentation_es:Instalacion#Requisitos_m.C3.ADnimos_hardware)

Pandora FMS nos pide tener con un hardware que cumpla con las siguientes
características. 

![requisitos-sistema](./images/requisitos-pandora.png)

El no tener un sistema que cumpla con los requisitos de los desarrolladores
podría acarrear diferentes problemas en el rendimiento de la aplicación.

Como experiencia, se necesita la RAM suficiente porque MySQL y los distintos 
servicios de la aplicación suelen consumir mucha memoria RAM. 

![memoria RAM](./images/ram-meme.jpg)


## Modificar la conexión ssh (si se conecta a un servidor remoto)

Como nos vamos a conectar a un servidor remoto tenemos que configurar la
conectividad para no perder la conexión de la máquina y que no puedan iniciar
sesión con las credenciales de usuario.

Para la conexión tenemos que modificar los siguientes archivos y que tengan las
siguientes líneas

`sudo vi /etc/ssh/sshd_config`

~~~
TCPKeepAlive yes 
ClientAliveInterval 10
~~~

`sudo vi /etc/ssh/ssh_config`

~~~
TCPKeepAlive yes 
ServerAliveInterval 10
~~~

Para que únicamente iniciemos sesión con nuestra llave privada mediante ssh, 
tenemos que tener nuestra llave pública en el servidor, para ellos sugiero revisar
este [link](https://www.cyberciti.biz/faq/how-to-disable-ssh-password-login-on-linux/)
y que en nuestro archivo `/etc/ssh/sshd_config` tenga estos parámetros.

~~~
ChallengeResponseAuthentication no
PasswordAuthentication no
UsePAM no
PermitRootLogin no
~~~

Reiniciamos los servicios de cliente y el demonio ssh

~~~
sudo systemctl restart ssh 
sudo systemctl restart sshd
~~~

## Modificar la lista de descarga de paquetes

Tenemos que modificar la lista de paquetes para la instalación de programas.
Abrimos el archivo `/etc/apt/sources.list`

`sudo vi /etc/apt/sources.list`


~~~
deb http://deb.debian.org/debian buster main contrib non-free 
deb-src http://deb.debian.org/debian buster main contrib non-free 
deb http://deb.debian.org/debian buster-updates main contrib non-free
deb-src http://deb.debian.org/debian buster-updates main contrib non-free
~~~

Y nos fijamos que tengan esas líneas

Actualizamos apt

~~~
sudo apt-get update && sudo apt-get upgrade
~~~

## Modificar hostname del equipo

Como contamos con un dominio propio para este servidor y que va a proveeer un
servicio de correo tenemos que configurar el hostname. Esto con la intención de
que el correo salga con el dominio de este servidor. 

Si usted no cuenta con un dominio propio para el servidor puede ver otras
opciones por ejemplo como hacer un relay de un 
[correo gmail](https://pandorafms.com/docs/index.php?title=Pandora:Configuracion_alertas_emails) 
para Pandora FMS

Ejecutamos 

`sudo hostnamectl set-hostname monitoring.redes.tonejito.cf`

Y modificar todo donde el antiguo nombre aparezca en `/etc/hosts` por el nuevo
nombre.

Reiniciamos el servidor

`sudo reboot`

# Requisitos para Pandora FMS

Requisitos mínimos para debian 10.

|||
|--|--|
|RAM:| 4GB| 
|CPU:| 1 nucleo a 2GHz |
|Disco Duro: |20GB mínimo a 7200rpm|

**Nota: Tener en cuenta que PandoraFMS usa Mysql:5.5 y PHP7> para sus nuevas
versiones.**

**NOTA MÁS IMPORTANTE: Puede que nos aparezcan varios errores a lo largo de todos estos procesos
de la instalación sobre falta de dependencias, ejecutar el siguiente comando `sudo apt-get --fix-broken install`
o bien `sudo apt-get -f install`**

Crearé una carpeta para los archivos que descargaré.

`mkdir deb && cd deb`

## Instalación de MySQL 

Pra más información de instalar MySQL visite el 
[link](https://www.digitalocean.com/community/tutorials/how-to-install-the-latest-mysql-on-debian-10)

~~~
sudo apt install gnupg
wget http://repo.mysql.com/mysql-apt-config_0.8.13-1_all.deb 
sudo dpkg -i mysql-apt-config_0.8.13-1_all.deb
~~~

Instalar MySQL-5.7

![mysql-version](./images/mysql-version.jpg)

Tenemos que cambiar la primera opción por MySQL 5.7. Si intenta hacerlo con 
la versión 8, no garantizamos que funcione, ya que no la probamos con la versión 5.7. 

Le damos **OK** y seguimos los pasos de la instalación.

Después nos aseguramos que se instalaron las dependencias faltantes
e instalamos mysql-server.

~~~
sudo apt update 
sudo apt-get -f install 
sudo apt install mysql-server
~~~

Configuramos MySQL e ingresamos una contraseña para *root*

~~~
sudo systemctl status mysql 
sudo mysql_secure_installation
~~~

“Seguir los pasos de configuración de la contraseña”
En esta parte se deja al administrador de base de datos como la configure.

## Requisitos de pandora

Descargamos e instalamos paquetería que necesita Pandora FMS

~~~
wget https://sourceforge.net/projects/pandora/files/Tools%20and%20dependencies%20%28All%20versions%29/DEB%20Debian%2C%20Ubuntu/wmi-client_0112-1_amd64.deb
sudo dpkg -i wmi-client_0112-1_amd64.deb
~~~

~~~
sudo apt-get install snmp snmpd libtime-format-perl libxml-simple-perl libxml-twig-perl libdbi-perl libnetaddr-ip-perl libhtml-parser-perl wmi-client xprobe2 nmap libmail-sendmail-perl traceroute libio-socket-inet6-perl libhtml-tree-perl libsnmp-perl snmp-mibs-downloader libio-socket-multicast-perl libsnmp-perl libjson-perl libencode-locale-perl
~~~

Muy posiblemente marque errores de dependencias, hay que ejecutar el siguiente comando
e intentarlo nuevamente. 

`sudo apt-get -f install`

Descargamos `php-db` que necesita Pandora FMS

~~~
wget http://deb.debian.org/debian/pool/main/p/php-db/php-db_1.9.2-1_all.deb sudo
dpkg -i php-db_1.9.2-1_all.deb
~~~

Instalamos apache, php en su versión 7.3 y otros programas

~~~
sudo apt-get install php libapache2-mod-php apache2 mysql-server php-gd
php-mysql php-pear php-snmp php-db php-gettext graphviz mysql-client php-curl
php-xmlrpc php-ldap dbconfig-common
~~~

## Configuración del archivo de la base de datos

Tenemos que modificar un archivo de configuración de la base de datos, 
sino estarán saliendo varios errores en la consola de Pandora FMS.

`sudo vi /etc/mysql/conf.d/mysql.cnf `

Y agregamos al final lo siguiente:

~~~
[mysqld] 
sql_mode= ""
~~~

Puede que el archivo este ubicado en otro lugar, hay que buscarlo y agregar esas líneas


# Instalación de Pandora FMS e inicialización de Pandora Server 

El siguiente [link](https://pandorafms.com/docs/index.php?title=Pandora:Documentation_es:Instalacion) 
es sobre la documentación completa de la instalación de Pandora FMS en otros sistemas operativos

~~~
wget https://sourceforge.net/projects/pandora/files/Pandora%20FMS%207.0NG/746/Debian_Ubuntu/pandorafms.server_7.0NG.746.deb
wget https://sourceforge.net/projects/pandora/files/Pandora%20FMS%207.0NG/746/Debian_Ubuntu/pandorafms.console_7.0NG.746.deb
~~~

**Reomencdación: Instalar el certificado SSL/TLS con
[Cerbot](#instalacion-de-cerbot) para tener habilitado el https y hacer los
cambios de direcciones automáticamente, si lo hace posteriormente, tendrá que
modificar un par de líneas en /config.php de Pandora para deshabilitar la página
por defecto de Apache**

`sudo dpkg -i pandorafms.console_7.0NG.746.deb pandorafms.server_7.0NG.746.deb`

Para terminar la instalación ingresamos desde nuestro navegador a la url del seridor
por ejemplo `http://10.10.10.10/pandora_console`

Seguir los pasos y copiar la contrañesa que te dan, esa contraseña deberá de colocarse en el archivo
`/etc/pandora/pandora_server.conf`

y donde aparezca la siguiente línea

`dbpass pandora`

Sustituir `pandora` por la contraseña que te dieron.

Iniciar el servicio de pandora.

`sudo /etc/init.d/pandora_server start`

Debería aparecer algo asi:

~~~
Pandora FMS Server X.Y Build XXXX Copyright (c) 2004-2009 ArticaST
This program is OpenSource, licensed under the terms of GPL License version 2.
You can download latest versions and documentation at http://www.pandorafms.org 

 [*] Backgrounding Pandora FMS Server process.
 
Pandora Server is now running with PID 2085
~~~

El servicio tentacle suele iniciarse cuando se reinicia el servidor, pero si es la primera vez,
hay que iniciarlo manualmente ejecutando:

`sudo /etc/init.d/tentacle.serverd start`

## Instalación del Agente Software 

~~~
wget https://sourceforge.net/projects/pandora/files/Pandora%20FMS%207.0NG/746/Debian_Ubuntu/pandorafms.agent_unix_7.0NG.746.deb
sudo dpkg -i pandorafms.agent_unix_7.0NG.746.deb
~~~

# Instalacion de [Cerbot](https://certbot.eff.org/lets-encrypt/debianbuster-apache)

Para instalar Cerbot es muy sencillo, basta con ejecutar un par de comandos.
Hay que tener un nombre de dominio.

~~~
sudo apt-get install certbot python-certbot-apache 
sudo certbot --apache
~~~

Recomendamos hacer el cambio de direcciones con la herramienta automáticamente, 
si no fue el caso tendrá que configurar unas lineas del `/config.php` de Pandora FMS

# Instalación de alertas por Correo
El [link](https://docs.aws.amazon.com/ses/latest/DeveloperGuide/postfix.html) 
de la documentación para AWS, que es como se hizo en el proyecto.
Pero se puede hacer con un [correo gmail](https://pandorafms.com/docs/index.php?title=Pandora:Configuracion_alertas_emails)
sin necesidad de tener dominio.

## Prerequisitos

• Desinstalar sendmail

`sudo apt-get purge sendmail*`

• Instalar postfix

`sudo apt-get install mailutils sudo apt-get install postfix`

• Instalar SASL 

`sudo apt-get install libsasl2-modules`

## Configurar postfix

En la línea de comando ejecutar:

~~~
sudo postconf -e "relayhost = [email-smtp.us-east-1.amazonaws.com]:587" \ 
"smtp_sasl_auth_enable = yes" \
"smtp_sasl_security_options = noanonymous" \ 
"smtp_sasl_password_maps = hash:/etc/postfix/sasl_passwd" \ 
"smtp_use_tls = yes" \ 
"smtp_tls_security_level = encrypt" \ 
"smtp_tls_note_starttls_offer = yes"
~~~

Como es en AWS el relayhost va a cambiar dependiendo de donde este alojado el servidor

en el archivo `/etc/postfix/master.cf` Comentar la línea con un #

`-o smtp_fallback_relay=`

Si no esta esa línea, no hacer nada.

Crear o modificar el archivo `/etc/postfix/sasl_passwd`

~~~
[email-smtp.us-east-1.amazonaws.com]:587
USERNAME:PASSWD
~~~

El `USERNAME` y `PASSWD` son de AWS SES, que son distintos del usuario y contraseña de la cuenta.

Ejecutar los siguientes comandos:

~~~
sudo postmap hash:/etc/postfix/sasl_passwd

sudo chown root:root /etc/postfix/sasl_passwd /etc/postfix/sasl_passwd.db 
sudo chmod 0600 /etc/postfix/sasl_passwd /etc/postfix/sasl_passwd.db 
sudo postconf -e 'smtp_tls_CAfile = /etc/ssl/certs/ca-certificates.crt'
~~~

Reiniciamos y cargamos

`sudo postfix start; sudo postfix reload`

Comprobar el correo 

~~~
sendmail -f sender@ciencias.unam.mx to@ciencias.unam.mx 
From: Sender Name <sender@ciencias.unam.mx> 
Subject: Amazon SES Test                
This message was sent using Amazon SES.
. 
~~~

Revisamos los Parametros de `/etc/pandora/pandora_server.conf`


~~~ 
mta_address localhost 
#mta_port 25 
#mta_user myuser@mydomain.com 
#mta_pass mypassword 
#mta_auth LOGIN 
mta_from Pandora FMS <admin@monitoring.redes.tonejito.com>
~~~

`mta_from Pandora FMS <admin@monitoring.redes.tonejito.com>`
es la dirección con la que van a salir los correos, 
como tenemos un usuario en el servidor llamado `admin` y nuestro dominio.
Si no los mensajes pueden que lleguen a la bandeja de *spam*

### Postfix main.cf

A continuación se muestra el archivo de configuración de postfix

~~~
cat /etc/postfix/main.cf

smtpd_banner = $myhostname ESMTP $mail_name
(Debian/GNU) biff = no

append_dot_mydomain = yes

readme_directory = no

compatibility_level = 2

smtpd_tls_cert_file=/etc/ssl/certs/ssl-cert-snakeoil.pem
smtpd_tls_key_file=/etc/ssl/private/ssl-cert-snakeoil.key smtpd_use_tls=yes
smtpd_tls_session_cache_database = btree:${data_directory}/smtpd_scache
smtp_tls_session_cache_database = btree:${data_directory}/smtp_scache

smtpd_relay_restrictions = permit_mynetworks permit_sasl_authenticated
defer_unauth_destination myhostname = monitoring.redes.tonejito.cf alias_maps =
hash:/etc/aliases alias_database = hash:/etc/aliases myorigin = /etc/mailname
mydestination = $myhostname relayhost = [email-smtp.us-east-1.amazonaws.com]:587
mynetworks = 127.0.0.0/8 [::ffff:127.0.0.0]/104 [::1]/128 mailbox_size_limit = 0
recipient_delimiter = + inet_interfaces = all inet_protocols = all
smtp_tls_note_starttls_offer = yes smtp_tls_security_level = encrypt
smtp_sasl_password_maps = hash:/etc/postfix/sasl_passwd
smtp_sasl_security_options = noanonymous smtp_sasl_auth_enable = yes
smtp_use_tls = yes smtp_tls_CAfile = /etc/ssl/certs/ca-certificates.crt
~~~

Los parámetros importantes que hay que cambiar son los de `myhostname` y el nombre en `/etc/mailname`

~~~
cat /etc/mailname 
monitoring.redes.tonejito.cf
~~~

## Configuración de una alerta por Correo en Pandora FMS

Si hemos levantado la configuración adecuada para postfix con Amazon SES basta con poner el correo electrónico
al que van a llegar los correos y levantar las alertas hacia esa dirección email.
Más adelante se hablará al respecto de personalizar alertas. Pero si todo esta bien
las alertas se pueden mandar al correo con el que se registro el administrador
de la consola.

# Alertas Telegram

[Documentación oficial](https://pandorafms.com/blog/es/notificaciones-con-telegram-pandora-fms/)
Pero como estamos en Debian10, tenemos que hacer una instalación extra por las dependencias.

~~~
sudo apt install libz-dev zliblg-dev libreadline-dev libconfig-dev libssl-dev lua5.2 liblua5.2-dev libevent-dev libjansson-dev libpython-dev make git-core screen

git clone --recursive https://github.com/kenorb-contrib/tg.git

cd tg 
./configure 
make
~~~

Para ejecutar el programa:

`sudo bin/telegram-cli -k tg-server.pub`

Tenemos que configurar el número de telefono por el cuál se mandarán los
mensajes de telegram, nota: Pandora FMS usa los servicios y programas como
`root`, ya que a la hora de programar la alerta en pandora lo va a ejecutar como
`root` y se tiene que configurar el número de teléfono lanzando el comando con
`sudo`. Pero si ya lo esta ejecutando como root, no hay problema.

## Configuración de una alerta con Telegram en Pandora FMS

Los pasos para configurar una alerta con Telegram son:

1. **Crear el comando**: Se necesita el siguiente comando 
   `/home/admin/tg/bin/telegram-cli -W -e "msg @Username \"Alerta de Pandora FMS para verificar un agente\""`
   este nos ayudara a mandar un telegram desde la cuenta que se dio de alta,
   tenemos que ver que se cree y nos aparecerá en nuestra lista de comandos.

![Telegram comando](./images/telegra-comando.JPG)

2. **Crear la acción**: Una vez que el comando ya exista el comando, necesitamos
   crear una acción con ese comando y debemos darle la visibilidad de la acción,
   con esto nos referimos a que tipos de servicios son los que pueden ocupar esta
   acción. 

3. **Asignarlo a una alerta con un respectivo modulo**:  Una vez creado el agente y
   asignados los módulos, nos vamos a la sección de alertas que nos proporciona
   nuestro agente y creamos una alerta nueva; esta nos pedirá que le asignemos
   un modulo (este dependerá del programador), que le asignemos una acción (esta
   acción va ser la que creamos para mandar mensajes de Telegram), en que estado
   va mandar la alerta (Warring, Crital, Manual o incluso nosotros podremos
   definir una plantilla para cierto valor) y cuanto umbral de respuesta va
   tener nuestra alerta. Así es como se asigna una alerta en Telegram, de tal
   forma se puede hacer para cualquier comando y acción ya definida. 

# Alertas Twitter 
[Documentación oficial](https://pandorafms.com/blog/twitter-alerts/)

Tener instalado unzip

`sudo apt-get install unzip`

~~~
wget https://github.com/oysttyer/oysttyer/archive/master.zip 

unzip master.zip

cd oysttyer-master 
sudo ./oysttyer.pl
~~~

Configurar el código de la aplicación para poder mandar mensajes
De igual manera ejecuto el comando con `sudo` porque Pandora FMS lo va usar.



## Configuración de una alerta con Twitter en Pandora FMS

Para crear una alerta con Tiwtter necesitamos lo siguiente:

1. **Crear el comando**: Se necesita el siguiente comando 
   `echo "@NOMBREUSUARIO MENSAJE" | /home/user/tw/oysttyer-master/oysttyer.pl &>/dev/null`
   este nos ayudara a mandar un tweet desde nuestro usuario administrador,
   tenemos que ver que se cree y nos aparecerá en nuestra lista de comandos.

![Comando Twitter](./images/twitter-comando.JPG)

2. **Crear la acción**: Una vez que el comando ya exista el comando, necesitamos
   crear una acción con ese comando y debemos darle la visibilidad de la acción,
   con esto nos referimos a que tipos de servicios son los que pueden ocupar esta
   acción. 

3. **Asignarlo a una alerta con un respectivo modulo**:  Una vez creado el agente y
   asignados los módulos, nos vamos a la sección de alertas que nos proporciona
   nuestro agente y creamos una alerta nueva; esta nos pedirá que le asignemos
   un modulo (este dependerá del programador), que le asignemos una acción (esta
   acción va ser la que creamos para mandar mensajes de Twitter), en que estado
   va mandar la alerta (Warring, Crital, Manual o incluso nosotros podremos
   definir una plantilla para cierto valor) y cuanto umbral de respuesta va
   tener nuestra alerta. Así es como se asigna una alerta en Twitter, de tal
   forma se puede hacer para cualquier comando y acción ya definida. 

# Pasos para monitorear un servidor

**Primero tenemos que crear un agente**, en este podemos definir que tipo de
servicio es, cada cuanto tiempo va hacer la revisión, en que sistema operativo
va correr, podemos poner la dirección IP y asignarle un nombre, ponemos la
documentación oficial para cualquier duda específica.  

Documentación oficial para 
[crear agentes de Pandora FMS](https://pandorafms.com/docs/index.php?title=Pandora:Documentation_es:Configuracion_Agentes)

Después creado el agente le **asignamos un modulo o varios**, dependiendo de lo que
se requiera, se pueden cargar plantillas de modulos o crear un propio modulo
para ser asignado a un agente. Documentación oficial 
[para asignar modulos a agentes de Pandora FMS](https://pandorafms.com/docs/index.php?title=Pandora:Creacion_modulo_agente_y_remoto)

Para poder asignar una alerta a un modulo y que nuestro agente realize una
acción se divide en 3 pasos:

1. **Crear un comando**
   
2. **Crear una acción**, ya que tengamos el comando definido creamos la acción con
   el tipo de servicios que pueden ocupar esta acción puede ser solo definida
   para DataBase, Servers, etc, o bien, puede ser asignada a todos los servicios. 

3. **Crear una alerta**, esta alerta se crea asignandole una acción a un modulo, de
   tal forma que cuando el modulo se encuentre en un estado (establecido por el
   programador) esta mande un mensaje, correo, un tweet, etc. 
   [Documentación oficial para crear alertas de Pandora FMS](https://pandorafms.com/docs/index.php?title=Pandora:Documentation_es:Alertas)
